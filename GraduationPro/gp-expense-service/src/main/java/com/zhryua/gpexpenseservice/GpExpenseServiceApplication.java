package com.zhryua.gpexpenseservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GpExpenseServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(GpExpenseServiceApplication.class, args);
    }

}
