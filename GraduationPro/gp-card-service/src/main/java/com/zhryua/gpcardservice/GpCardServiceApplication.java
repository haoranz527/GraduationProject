package com.zhryua.gpcardservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GpCardServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(GpCardServiceApplication.class, args);
    }

}
