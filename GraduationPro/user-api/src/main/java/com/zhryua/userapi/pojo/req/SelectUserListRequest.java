package com.zhryua.userapi.pojo.req;

import lombok.Data;

import java.util.List;

/**
 * @author zhryua
 * 用户列表查询请求
 */
@Data
public class SelectUserListRequest extends PageRequest {

    String userId;

    String phone;

    String name;

    Integer department;

    Integer classNumber;

    Integer status;

    String cardId;

    String dormitoryId;

    List<String> ctms;

}
