package com.zhryua.userapi.pojo.database;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author 
 * 
 */
@Table(name="token")
@Data
public class Token implements Serializable {
    @Id
    @GeneratedValue
    private String bizId;

    private String token;

    private String userId;

    private Date utm;

    private Date ctm;

    private static final long serialVersionUID = 1L;
}