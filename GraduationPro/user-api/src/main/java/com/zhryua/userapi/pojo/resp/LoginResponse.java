package com.zhryua.userapi.pojo.resp;

import com.zhryua.userapi.pojo.TokenModel;
import lombok.Data;

@Data
public class LoginResponse extends BaseResponse{

    private String userId;

    private String token;

    public LoginResponse() {
        super();
    }

    public LoginResponse(Integer code, String msg) {
        super(code, msg);
    }

    public LoginResponse(Integer code, String msg, String userId, String token) {
        super(code, msg);
        this.userId = userId;
        this.token = token;
    }
}
