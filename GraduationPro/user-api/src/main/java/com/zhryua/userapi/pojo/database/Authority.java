package com.zhryua.userapi.pojo.database;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 * @author 
 * 
 */
@Table(name="authority")
@Data
public class Authority implements Serializable {
    @Id
    @GeneratedValue
    private String bizId;

    private Integer canInsert;

    private Integer canSearch;

    private Integer canDrop;

    private Integer canUpdate;

    private static final long serialVersionUID = 1L;
}