package com.zhryua.userapi.pojo.req;

import lombok.Data;

/**
 * @author zhryua
 */
@Data
public class PageRequest extends BaseRequest{

    int pageNum;

    int pageSize;

}
