package com.zhryua.userapi.pojo.req;

import lombok.Data;

@Data
public class UpdateUserRequest extends BaseRequest{

    private String userId;

    private Integer type;

    private String name;

    private String phone;

    private String password;

    private String email;

    private String qq;

    private String wechat;

    private Integer department;

    private Integer classNumber;

    private String identity;

    private Integer is_authentication;

    private Integer status;

    private Integer subApp;

    private String dormitoryId;

    private Short bed;

    private String image;

}
