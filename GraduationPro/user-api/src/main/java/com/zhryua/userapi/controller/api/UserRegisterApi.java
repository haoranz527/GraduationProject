package com.zhryua.userapi.controller.api;

import com.zhryua.userapi.pojo.req.*;
import com.zhryua.userapi.pojo.resp.BaseResponse;
import com.zhryua.userapi.pojo.resp.LoginResponse;
import com.zhryua.userapi.pojo.resp.SelectUserListResponse;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

//@RestController
//@RequestMapping("pri/v1/user")
public interface UserRegisterApi {


    @PostMapping("/pri/v1/user/login")
    public LoginResponse login(@RequestBody LoginRequest request);

    @PostMapping("/pri/v1/user/logout")
    public BaseResponse logout(HttpServletRequest request);

    @PostMapping("/pri/v1/user/selectUserPage")
    public SelectUserListResponse selectUserPage(@RequestBody SelectUserListRequest request);

    @PostMapping("/pri/v1/user/updateUser")
    public BaseResponse updateUser(@RequestBody UpdateUserRequest request);

    @PostMapping("/pri/v1/user/insert")
    public BaseResponse insertUser(@RequestBody InsertUserRequest request);

    @PostMapping("sqlTest")
    public String sqlTest(@RequestParam("id") String id);



}
