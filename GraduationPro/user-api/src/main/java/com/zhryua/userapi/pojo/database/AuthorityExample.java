package com.zhryua.userapi.pojo.database;

import java.util.ArrayList;
import java.util.List;

public class AuthorityExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public AuthorityExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andBizIdIsNull() {
            addCriterion("bizId is null");
            return (Criteria) this;
        }

        public Criteria andBizIdIsNotNull() {
            addCriterion("bizId is not null");
            return (Criteria) this;
        }

        public Criteria andBizIdEqualTo(String value) {
            addCriterion("bizId =", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdNotEqualTo(String value) {
            addCriterion("bizId <>", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdGreaterThan(String value) {
            addCriterion("bizId >", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdGreaterThanOrEqualTo(String value) {
            addCriterion("bizId >=", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdLessThan(String value) {
            addCriterion("bizId <", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdLessThanOrEqualTo(String value) {
            addCriterion("bizId <=", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdLike(String value) {
            addCriterion("bizId like", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdNotLike(String value) {
            addCriterion("bizId not like", value, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdIn(List<String> values) {
            addCriterion("bizId in", values, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdNotIn(List<String> values) {
            addCriterion("bizId not in", values, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdBetween(String value1, String value2) {
            addCriterion("bizId between", value1, value2, "bizId");
            return (Criteria) this;
        }

        public Criteria andBizIdNotBetween(String value1, String value2) {
            addCriterion("bizId not between", value1, value2, "bizId");
            return (Criteria) this;
        }

        public Criteria andCanInsertIsNull() {
            addCriterion("canInsert is null");
            return (Criteria) this;
        }

        public Criteria andCanInsertIsNotNull() {
            addCriterion("canInsert is not null");
            return (Criteria) this;
        }

        public Criteria andCanInsertEqualTo(Integer value) {
            addCriterion("canInsert =", value, "canInsert");
            return (Criteria) this;
        }

        public Criteria andCanInsertNotEqualTo(Integer value) {
            addCriterion("canInsert <>", value, "canInsert");
            return (Criteria) this;
        }

        public Criteria andCanInsertGreaterThan(Integer value) {
            addCriterion("canInsert >", value, "canInsert");
            return (Criteria) this;
        }

        public Criteria andCanInsertGreaterThanOrEqualTo(Integer value) {
            addCriterion("canInsert >=", value, "canInsert");
            return (Criteria) this;
        }

        public Criteria andCanInsertLessThan(Integer value) {
            addCriterion("canInsert <", value, "canInsert");
            return (Criteria) this;
        }

        public Criteria andCanInsertLessThanOrEqualTo(Integer value) {
            addCriterion("canInsert <=", value, "canInsert");
            return (Criteria) this;
        }

        public Criteria andCanInsertIn(List<Integer> values) {
            addCriterion("canInsert in", values, "canInsert");
            return (Criteria) this;
        }

        public Criteria andCanInsertNotIn(List<Integer> values) {
            addCriterion("canInsert not in", values, "canInsert");
            return (Criteria) this;
        }

        public Criteria andCanInsertBetween(Integer value1, Integer value2) {
            addCriterion("canInsert between", value1, value2, "canInsert");
            return (Criteria) this;
        }

        public Criteria andCanInsertNotBetween(Integer value1, Integer value2) {
            addCriterion("canInsert not between", value1, value2, "canInsert");
            return (Criteria) this;
        }

        public Criteria andCanSearchIsNull() {
            addCriterion("canSearch is null");
            return (Criteria) this;
        }

        public Criteria andCanSearchIsNotNull() {
            addCriterion("canSearch is not null");
            return (Criteria) this;
        }

        public Criteria andCanSearchEqualTo(Integer value) {
            addCriterion("canSearch =", value, "canSearch");
            return (Criteria) this;
        }

        public Criteria andCanSearchNotEqualTo(Integer value) {
            addCriterion("canSearch <>", value, "canSearch");
            return (Criteria) this;
        }

        public Criteria andCanSearchGreaterThan(Integer value) {
            addCriterion("canSearch >", value, "canSearch");
            return (Criteria) this;
        }

        public Criteria andCanSearchGreaterThanOrEqualTo(Integer value) {
            addCriterion("canSearch >=", value, "canSearch");
            return (Criteria) this;
        }

        public Criteria andCanSearchLessThan(Integer value) {
            addCriterion("canSearch <", value, "canSearch");
            return (Criteria) this;
        }

        public Criteria andCanSearchLessThanOrEqualTo(Integer value) {
            addCriterion("canSearch <=", value, "canSearch");
            return (Criteria) this;
        }

        public Criteria andCanSearchIn(List<Integer> values) {
            addCriterion("canSearch in", values, "canSearch");
            return (Criteria) this;
        }

        public Criteria andCanSearchNotIn(List<Integer> values) {
            addCriterion("canSearch not in", values, "canSearch");
            return (Criteria) this;
        }

        public Criteria andCanSearchBetween(Integer value1, Integer value2) {
            addCriterion("canSearch between", value1, value2, "canSearch");
            return (Criteria) this;
        }

        public Criteria andCanSearchNotBetween(Integer value1, Integer value2) {
            addCriterion("canSearch not between", value1, value2, "canSearch");
            return (Criteria) this;
        }

        public Criteria andCanDropIsNull() {
            addCriterion("canDrop is null");
            return (Criteria) this;
        }

        public Criteria andCanDropIsNotNull() {
            addCriterion("canDrop is not null");
            return (Criteria) this;
        }

        public Criteria andCanDropEqualTo(Integer value) {
            addCriterion("canDrop =", value, "canDrop");
            return (Criteria) this;
        }

        public Criteria andCanDropNotEqualTo(Integer value) {
            addCriterion("canDrop <>", value, "canDrop");
            return (Criteria) this;
        }

        public Criteria andCanDropGreaterThan(Integer value) {
            addCriterion("canDrop >", value, "canDrop");
            return (Criteria) this;
        }

        public Criteria andCanDropGreaterThanOrEqualTo(Integer value) {
            addCriterion("canDrop >=", value, "canDrop");
            return (Criteria) this;
        }

        public Criteria andCanDropLessThan(Integer value) {
            addCriterion("canDrop <", value, "canDrop");
            return (Criteria) this;
        }

        public Criteria andCanDropLessThanOrEqualTo(Integer value) {
            addCriterion("canDrop <=", value, "canDrop");
            return (Criteria) this;
        }

        public Criteria andCanDropIn(List<Integer> values) {
            addCriterion("canDrop in", values, "canDrop");
            return (Criteria) this;
        }

        public Criteria andCanDropNotIn(List<Integer> values) {
            addCriterion("canDrop not in", values, "canDrop");
            return (Criteria) this;
        }

        public Criteria andCanDropBetween(Integer value1, Integer value2) {
            addCriterion("canDrop between", value1, value2, "canDrop");
            return (Criteria) this;
        }

        public Criteria andCanDropNotBetween(Integer value1, Integer value2) {
            addCriterion("canDrop not between", value1, value2, "canDrop");
            return (Criteria) this;
        }

        public Criteria andCanUpdateIsNull() {
            addCriterion("canUpdate is null");
            return (Criteria) this;
        }

        public Criteria andCanUpdateIsNotNull() {
            addCriterion("canUpdate is not null");
            return (Criteria) this;
        }

        public Criteria andCanUpdateEqualTo(Integer value) {
            addCriterion("canUpdate =", value, "canUpdate");
            return (Criteria) this;
        }

        public Criteria andCanUpdateNotEqualTo(Integer value) {
            addCriterion("canUpdate <>", value, "canUpdate");
            return (Criteria) this;
        }

        public Criteria andCanUpdateGreaterThan(Integer value) {
            addCriterion("canUpdate >", value, "canUpdate");
            return (Criteria) this;
        }

        public Criteria andCanUpdateGreaterThanOrEqualTo(Integer value) {
            addCriterion("canUpdate >=", value, "canUpdate");
            return (Criteria) this;
        }

        public Criteria andCanUpdateLessThan(Integer value) {
            addCriterion("canUpdate <", value, "canUpdate");
            return (Criteria) this;
        }

        public Criteria andCanUpdateLessThanOrEqualTo(Integer value) {
            addCriterion("canUpdate <=", value, "canUpdate");
            return (Criteria) this;
        }

        public Criteria andCanUpdateIn(List<Integer> values) {
            addCriterion("canUpdate in", values, "canUpdate");
            return (Criteria) this;
        }

        public Criteria andCanUpdateNotIn(List<Integer> values) {
            addCriterion("canUpdate not in", values, "canUpdate");
            return (Criteria) this;
        }

        public Criteria andCanUpdateBetween(Integer value1, Integer value2) {
            addCriterion("canUpdate between", value1, value2, "canUpdate");
            return (Criteria) this;
        }

        public Criteria andCanUpdateNotBetween(Integer value1, Integer value2) {
            addCriterion("canUpdate not between", value1, value2, "canUpdate");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}