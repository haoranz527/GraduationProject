package com.zhryua.userapi.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class TokenModel {

    private String userId;

    private String token;

    private Date date;

    public TokenModel(String userId, String token, Date date) {
        this.userId = userId;
        this.token = token;
        this.date = date;
    }

    public TokenModel(String userId, String token) {
        this.token = token;
        this.userId = userId;
    }
}
