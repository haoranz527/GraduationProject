package com.zhryua.userapi.pojo.resp;


import com.zhryua.userapi.pojo.info.UserInfo;
import lombok.Data;

import java.util.List;

/**
 * @author zhryua
 * 用户列表查询请求
 */
@Data
public class SelectUserListResponse extends PageResponse{

    private List<UserInfo> data;

    public SelectUserListResponse(){

    }

    /**
     * 查询成功时返回
     * @param code
     * @param msg
     * @param totalPage
     * @param pageNum
     * @param pageSize
     * @param nextPage
     * @param prePage
     * @param data
     */
    public SelectUserListResponse(Integer code
            , String msg
            , Integer totalPage
            , Integer pageNum
            , Integer pageSize
            , Integer nextPage
            , Integer prePage
            , List<UserInfo> data) {
        this.setCode(code);
        this.setMsg(msg);
        this.setTotalPage(totalPage);
        this.setPrePage(prePage);
        this.setNextPage(nextPage);
        this.setPageNum(pageNum);
        this.setPageSize(pageSize);
        this.data = data;
    }

    /**
     * 查询失败 或 查询为空 时使用此构造函数
     * @param code
     * @param msg
     */
    public SelectUserListResponse(Integer code, String msg) {
        this.setCode(code);
        this.setMsg(msg);
    }

}
