package com.zhryua.userapi.pojo.req;

import lombok.Data;

@Data
public class InsertUserRequest extends BaseRequest{

    private String name;

    private String phone;

    private String password;

    private String email;

    private String qq;

    private String wechat;

    private Integer department;

    private Integer classNumber;

    private String identity;

    private String dormitoryId;

    private Short bed;

    private String userId;

    private String image;

}
