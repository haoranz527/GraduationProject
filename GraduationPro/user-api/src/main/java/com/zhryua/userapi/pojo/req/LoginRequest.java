package com.zhryua.userapi.pojo.req;

import jdk.jfr.Description;
import lombok.Data;

@Data
public class LoginRequest extends BaseRequest{

    private String userId;

    private String password;

    @Description(value = "0-当天免登录，1-7天免登录")
    private Integer type;

}
