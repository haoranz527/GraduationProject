package com.zhryua.userapi.pojo.info;

import lombok.Data;

import java.util.Date;

@Data
public class UserInfo {

    private String bizId;

    private String name;

    private String phone;

    private String password;

    private String email;

    private String qq;

    private String wechat;

    private Integer department;

    private Integer classNumber;

    private String identity;

    private Integer is_authentication;

    private Date ctm;

    private Date utm;

    private Integer status;

    private String cardId;

    private String auditorId;

    private Integer subApp;

    private String dormitoryId;

    private Short bed;

    private String userId;

    private String image;

}
