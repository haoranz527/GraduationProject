package com.zhryua.userapi.pojo.database;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 * @author 
 * 
 */
@Table(name="role")
@Data
public class Role implements Serializable {
    @Id
    @GeneratedValue
    private String bizId;

    private String name;

    private static final long serialVersionUID = 1L;
}