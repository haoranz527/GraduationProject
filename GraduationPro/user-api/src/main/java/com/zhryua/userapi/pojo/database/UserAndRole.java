package com.zhryua.userapi.pojo.database;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 * @author 
 * 
 */
@Table(name="user_and_role")
@Data
public class UserAndRole implements Serializable {
    private String userId;

    private String roleId;

    private static final long serialVersionUID = 1L;
}