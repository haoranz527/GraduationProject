package com.zhryua.userapi.pojo.resp;

import lombok.Data;

@Data
public class PageResponse extends BaseResponse{

    private Integer totalPage;

    private Integer pageNum;

    private Integer pageSize;

    private Integer nextPage;

    private Integer prePage;

}
