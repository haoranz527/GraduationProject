package com.zhryua.userapi.pojo.resp;

import lombok.Data;

@Data
public class BaseResponse {

    private Integer code;

    private String msg;

    public BaseResponse() {

    }

    public BaseResponse(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
