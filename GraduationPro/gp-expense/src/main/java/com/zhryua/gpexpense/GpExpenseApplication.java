package com.zhryua.gpexpense;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GpExpenseApplication {

    public static void main(String[] args) {
        SpringApplication.run(GpExpenseApplication.class, args);
    }

}
