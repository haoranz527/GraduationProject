package com.zhryua.gpuserservice.other;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zhryua.gpuserservice.mapper.UserMapper;
import com.zhryua.userapi.pojo.database.User;
import com.zhryua.userapi.pojo.database.UserExample;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class PageHelperTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void test_1() {
        PageHelper.startPage(20,4);
        List<User> list = userMapper.selectByExample(new UserExample());
        PageInfo<User> pageInfo = new PageInfo<>(list);
        List<User> userList = pageInfo.getList();
        System.out.println("Size: " + userList.size());
        for (User user : userList) {
            System.out.println(ToStringBuilder.reflectionToString(user));
        }
    }

}
