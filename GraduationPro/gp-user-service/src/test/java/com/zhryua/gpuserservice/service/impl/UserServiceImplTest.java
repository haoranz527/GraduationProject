package com.zhryua.gpuserservice.service.impl;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class UserServiceImplTest {
    @Test
    public void getRandomDigits_test() {
        System.out.println(getRandomDigits(6));
    }

    @Test
    public void getRandomDigits_test_2() {
        for (int i = 0; i < 100; i++) {
            System.out.println(getRandomDigits(6));
        }
    }

    public String getRandomDigits(int len) {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < len; i++) {
            res.append((int) (Math.random() * 10));
        }
        return res.toString();
    }

}
