package com.zhryua.gpuserservice.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zhryua.gpuserservice.mapper.UserMapper;
import com.zhryua.gpuserservice.utils.redis.RedisUtil;
import com.zhryua.userapi.pojo.database.User;
import com.zhryua.userapi.pojo.database.UserExample;
import com.zhryua.userapi.pojo.info.UserInfo;
import com.zhryua.userapi.pojo.req.InsertUserRequest;
import com.zhryua.userapi.pojo.req.LoginRequest;
import com.zhryua.userapi.pojo.req.SelectUserListRequest;
import com.zhryua.userapi.pojo.req.UpdateUserRequest;
import com.zhryua.userapi.pojo.resp.BaseResponse;
import com.zhryua.userapi.pojo.resp.LoginResponse;
import com.zhryua.userapi.pojo.resp.SelectUserListResponse;
import com.zhryua.utilsapi.StringUtils.IdWorker;
import com.zhryua.utilsapi.StringUtils.MD5Utils;
import com.zhryua.utilsapi.StringUtils.MatchString;
import com.zhryua.utilsapi.YUAConstants.YUAConstants;
import com.zhryua.utilsapi.token.TokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class UserService{

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RedisUtil redisUtil;

    private static final IdWorker idWorker = new IdWorker();


    /**
     * 2022年3月11日10:02:56重构
     * @param request
     * @return
     */
    public LoginResponse login(LoginRequest request) {
        if(ObjectUtils.isEmpty(request)) {
            return new LoginResponse(YUAConstants.ERROR, "request is null");
        }
        try {
            UserExample userExample = new UserExample();
            UserExample.Criteria criteria = userExample.createCriteria();
            if(!ObjectUtils.isEmpty(request.getPassword())) {
                criteria.andPasswordEqualTo(MD5Utils.string2MD5(request.getPassword().trim()));
            }
            if(!ObjectUtils.isEmpty(request.getUserId())) {
                criteria.andUserIdEqualTo(request.getUserId().trim());
            }
            long count = userMapper.selectByExample(userExample).size();
            System.out.println("count " + count);
            if(count != YUAConstants.ZERO_INT) {
                String token = TokenUtil.getToken();
                // 将token - userid存入缓存，默认登录状态为7天。
                long time = YUAConstants.SEVEN_DAY;
                if(ObjectUtils.isNotEmpty(request.getType()) && request.getType().equals(YUAConstants.ZERO_INT)) {
                    time = YUAConstants.ONE_DAY;
                }
                redisUtil.setString(token, request.getUserId(), time);
                return new LoginResponse(YUAConstants.SUCCESS, "SUCCESS", request.getUserId(), token);
            }
            return new LoginResponse(YUAConstants.ERROR, "内部错误");
        } catch (Exception e) {
            return new LoginResponse(YUAConstants.ERROR, "内部错误");
        }

//        try{
//            String userId = request.getUserId();
//            String password = request.getPassword();
//            UserExample userExample = new UserExample();
//            userExample.createCriteria()
//                    .andUserIdEqualTo(userId)
//                    .andPasswordEqualTo(MD5Utils.string2MD5(password));
//            List<User> userList = userMapper.selectByExample(userExample);
//            if(CollectionUtils.isNotEmpty(userList)) {
//                User user = userList.get(0);
//                // 这里时间设置为1天
//                TokenModel tokenModel = tokenHelper.create(user.getUserId(), YUAConstants.ONE_DAY);
//                return new LoginResponse(YUAConstants.SUCCESS, "success", tokenModel);
//            }
//            return new LoginResponse(YUAConstants.ERROR, "内部错误");
//        } catch (Exception e) {
//            e.printStackTrace();
//            return new LoginResponse(YUAConstants.ERROR, "内部错误");
//        }
    }



    public BaseResponse logout(HttpServletRequest request) {
        if(ObjectUtils.isEmpty(request)) {
            return new BaseResponse(YUAConstants.ERROR, "request is null");
        }
        try{
            String token = request.getHeader("token");
            if(StringUtils.isNotEmpty(token)) {
                redisUtil.delete(token);
                return new BaseResponse(YUAConstants.SUCCESS, "SUCCESS");
            }
            return new BaseResponse(YUAConstants.ERROR, "内部错误");
        } catch (Exception e) {
            e.printStackTrace();
            return new BaseResponse(YUAConstants.ERROR, "内部错误");
        }
    }

 
    public SelectUserListResponse selectUserPage(SelectUserListRequest request) {
        log.info("####入参{}####", request);
        try{
            if(Objects.isNull(request)) {
                log.error("####传参为空####");
                return new SelectUserListResponse(YUAConstants.ERROR, "request is NULL !");
            }
            // 分页查找
            if(!Objects.equals(request.getPageNum(), YUAConstants.MINUS_ONE_INT)
                    && !Objects.equals(request.getPageSize(), YUAConstants.MINUS_ONE_INT)) {
                PageHelper.startPage(request.getPageSize(), request.getPageNum());
            }
            UserExample userExample = new UserExample();
            UserExample.Criteria criteria = userExample.createCriteria();
            // 用户id
            if(!StringUtils.isEmpty(request.getUserId())) {
                criteria.andUserIdEqualTo(request.getUserId().trim());
            }
            // 手机号
            if(!StringUtils.isEmpty(request.getPhone())) {
                criteria.andPhoneEqualTo(request.getPhone().trim());
            }
            // 姓名
            if(!StringUtils.isEmpty(request.getName())) {
                criteria.andNameEqualTo(request.getName().trim());
            }
            // 院系
            if(ObjectUtils.isNotEmpty(request.getDepartment()) && request.getDepartment() != YUAConstants.MINUS_ONE_INT) {
                criteria.andDepartmentEqualTo(request.getDepartment());
            }
            // 班级
            if(ObjectUtils.isNotEmpty(request.getClassNumber()) && request.getClassNumber() != YUAConstants.MINUS_ONE_INT) {
                criteria.andClassNumberEqualTo(request.getClassNumber());
            }
            // 状态
            if(ObjectUtils.isNotEmpty(request.getStatus()) && request.getStatus() != YUAConstants.MINUS_ONE_INT) {
                criteria.andStatusEqualTo(request.getStatus());
            }
            // 校园卡Id
            if(!StringUtils.isEmpty(request.getCardId())) {
                criteria.andCardIdEqualTo(request.getCardId().trim());
            }
            // 宿舍号
            if(!StringUtils.isEmpty(request.getDormitoryId())) {
                criteria.andDormitoryIdEqualTo(request.getDormitoryId().trim());
            }
            // 创建时间区间
            if(!CollectionUtils.isEmpty(request.getCtms())) {
                criteria.andCtmBetween(new SimpleDateFormat().parse(request.getCtms().get(0))
                        , new SimpleDateFormat().parse(request.getCtms().get(1)));
            }
            long count = userMapper.countByExample(userExample);
            if(count > YUAConstants.ZERO_INT) {
                List<User> userList = userMapper.selectByExample(userExample);
                List<UserInfo> userInfoList = new ArrayList<>();
                // Bean类型转换
                for (User userCur : userList) {
                    UserInfo userInfo = new UserInfo();
                    BeanUtils.copyProperties(userCur, userInfo);
                    userInfoList.add(userInfo);
                }
                if(!userInfoList.isEmpty()) {
                    PageInfo<UserInfo> pageInfo = new PageInfo<>(userInfoList);
                    Integer code = YUAConstants.SUCCESS;
                    String msg = "SUCCESS";
                    Integer totalPage = pageInfo.getPages();
                    Integer pageNum = pageInfo.getPageNum();
                    Integer pageSize = pageInfo.getPageSize();
                    Integer nextPage = pageInfo.getNextPage();
                    Integer prePage = pageInfo.getPrePage();
                    return new SelectUserListResponse(code, msg, totalPage, pageNum, pageSize, nextPage, prePage, pageInfo.getList());
                } else {
                    return new SelectUserListResponse(YUAConstants.SUCCESS, "userInfoList is NULL !");
                }
            } else {
                return new SelectUserListResponse(YUAConstants.SUCCESS, "count is ZERO !");
            }
        } catch (Exception e) {
            log.error("####{}####", e);

            return new SelectUserListResponse(YUAConstants.ERROR, "内部错误");
        }
    }

 
    public BaseResponse updateUser(UpdateUserRequest request) {
        log.info("####入参{}####", request);

        try{
            if(ObjectUtils.isEmpty(request)) {
                return new BaseResponse(YUAConstants.ERROR, "request is NULL !");
            }

            UserExample userExample = new UserExample();
            userExample.createCriteria().andUserIdEqualTo(request.getUserId());
            List<User> users = userMapper.selectByExample(userExample);
            User user = users.get(0);
            log.info("####user原信息：{}####", user);

            // 请求类型为2，即删除数据请求
            if(ObjectUtils.isNotEmpty(request.getType()) && request.getType() == YUAConstants.TWO_INT) {
                user.setStatus(YUAConstants.TWO_INT);
                userMapper.updateByPrimaryKey(users.get(0));
                return new BaseResponse(YUAConstants.SUCCESS, "用户已注销");
            } else { // 更新数据
                if(ObjectUtils.isNotEmpty(request.getName())) {
                    user.setName(request.getName().trim());
                }
                if(ObjectUtils.isNotEmpty(request.getPhone())) {
                    if(MatchString.isMobile(request.getPhone().trim())) {
                        user.setPhone(request.getPhone().trim());
                    } else { // 手机号没有通过正则表达式
                        return new BaseResponse(YUAConstants.ERROR, "phone is not legal !");
                    }
                }
                if(ObjectUtils.isNotEmpty(request.getPassword().trim())) {
                    user.setPassword(MD5Utils.string2MD5(request.getPassword().trim()));
                }
                if(ObjectUtils.isNotEmpty(request.getEmail().trim())) {
                    if(MatchString.isEmail(request.getEmail().trim())) {
                        user.setEmail(request.getEmail().trim());
                    } else {
                        return new BaseResponse(YUAConstants.ERROR, "email is not legal !");
                    }
                }
                if(ObjectUtils.isNotEmpty(request.getQq().trim())) {
                    user.setQq(request.getQq().trim());
                }
                if(ObjectUtils.isNotEmpty(request.getWechat().trim())) {
                    user.setWechat(request.getWechat().trim());
                }
                if(ObjectUtils.isNotEmpty(request.getDepartment())
                        && user.getDepartment() > YUAConstants.MINUS_ONE_INT) {
                    user.setDepartment(request.getDepartment());
                }
                if(ObjectUtils.isNotEmpty(request.getClassNumber())
                        && request.getClassNumber() > YUAConstants.MINUS_ONE_INT) {
                    user.setClassNumber(request.getClassNumber());
                }
                if(ObjectUtils.isNotEmpty(request.getIdentity().trim())) {
                    user.setIdentity(request.getIdentity().trim());
                }
                if(ObjectUtils.isNotEmpty(request.getIs_authentication())
                        && request.getIs_authentication() > YUAConstants.MINUS_ONE_INT) {
                    user.setIs_authentication(request.getIs_authentication());
                }
                if(ObjectUtils.isNotEmpty(request.getStatus())
                        && request.getStatus() > YUAConstants.MINUS_ONE_INT) {
                    user.setStatus(request.getStatus());
                }
                if(ObjectUtils.isNotEmpty(request.getSubApp())
                        && request.getSubApp() > YUAConstants.MINUS_ONE_INT) {
                    user.setSubApp(request.getSubApp());
                }
                if(ObjectUtils.isNotEmpty(request.getDormitoryId().trim())) {
                    user.setDormitoryId(request.getDormitoryId().trim());
                }
                if(ObjectUtils.isNotEmpty(request.getBed())
                        && request.getBed() > YUAConstants.MINUS_ONE_INT) {
                    user.setBed(request.getBed());
                }
                if(ObjectUtils.isNotEmpty(request.getImage().trim())) {
                    user.setImage(request.getImage().trim());
                }

                int update = userMapper.updateByPrimaryKey(user);

                if(update <= YUAConstants.ZERO_INT) {
                    return new BaseResponse(YUAConstants.ERROR, "更新失败！！！");
                }

                return new BaseResponse(YUAConstants.SUCCESS, "SUCCESS");
            }
        } catch (Exception e) {
            log.error("####{}####", e);

            return new BaseResponse(YUAConstants.ERROR, "内部错误");
        }
    }

 
    public BaseResponse insertUser(InsertUserRequest request) {
        log.info("####入参{}####", request);
        try{
            if(Objects.isNull(request)) {
                return new BaseResponse(YUAConstants.ERROR, "request is NULL !");
            }
            User user = new User();
            // 姓名
            if(!Objects.isNull(request.getName())) {
                user.setName(request.getName().trim());
            }
            // 手机号
            if(!Objects.isNull(request.getPhone())) {
                user.setPhone(request.getPhone().trim());
            }
            // 密码
            String pwd = null;
            if(!Objects.isNull(request.getPassword())) {
                String md5 = MD5Utils.string2MD5(request.getPassword().trim());
                pwd = md5;
                user.setPassword(md5);
            }
            // email
            if(!Objects.isNull(request.getEmail())) {
                user.setEmail(request.getEmail().trim());
            }
            // qq
            if(!Objects.isNull(request.getQq())) {
                user.setQq(request.getQq().trim());
            }
            // 微信
            if(!Objects.isNull(request.getWechat())) {
                user.setWechat(request.getWechat().trim());
            }
            // 院系
            if(!Objects.isNull(request.getDepartment())
                    && request.getDepartment() > YUAConstants.MINUS_ONE_INT) {
                user.setDepartment(request.getDepartment());
            }
            // 班级
            if(!Objects.isNull(request.getClassNumber())
                    && request.getClassNumber() > YUAConstants.MINUS_ONE_INT) {
                user.setClassNumber(request.getClassNumber());
            }
            // 身份证号
            if(!Objects.isNull(request.getIdentity())) {
                user.setIdentity(request.getIdentity().trim());
            }
            // 宿舍号
            if(!Objects.isNull(request.getDormitoryId())) {
                user.setDormitoryId(request.getDormitoryId().trim());
            }
            // 床位号
            if(!Objects.isNull(request.getBed()) && request.getBed() > YUAConstants.MINUS_ONE_INT) {
                user.setBed(request.getBed());
            }
            // 用户Id
//            if(!Objects.isNull(request.getUserId().trim())) {
//                UserExample userExample = new UserExample();
//                userExample.createCriteria().andUserIdEqualTo(request.getUserId().trim());
//                long count = userMapper.countByExample(userExample);
//                if(count == YUAConstants.ZERO_INT) {
//                    user.setUserId(request.getUserId().trim());
//                } else {
//                    return new BaseResponse(YUAConstants.ERROR, "userid was used !");
//                }
//            }
            // 头像图片地址
            if(!Objects.isNull(request.getImage())) {
                user.setImage(request.getImage().trim());
            }

            // 设置Id
            String sBizId = "biz" + idWorker.nextId();
            String sCardId = "C" + idWorker.nextId();
            String sUserId = "U" + idWorker.nextId();
            String sAuditorId = "A" + idWorker.nextId();
            user.setBizId(sBizId);
            user.setCardId(sCardId);
            user.setAuditorId(sAuditorId);
            user.setUserId(sUserId);

            // todo 发送身份验证码【手机短信验证,个人无法实现】
            user.setIs_authentication(0);

            user.setCtm(new Date());
            user.setUtm(new Date());
            user.setStatus(3);
            user.setSubApp(3);
            int i = userMapper.insert(user);

            if(i == YUAConstants.ONE_INT) {
                return new BaseResponse(YUAConstants.SUCCESS, "SUCCESS");
            } else {
                return new BaseResponse(YUAConstants.ERROR, "插入异常");
            }

        } catch (Exception e) {
            e.printStackTrace();

            log.error("####{}####", e);

            return new BaseResponse(YUAConstants.ERROR, e.getMessage());
        }
    }

 
    public String sqlTest(String id) {

        UserExample userExample = new UserExample();

        userExample.createCriteria().andUserIdEqualTo(id);

        List<User> users = userMapper.selectByExample(userExample);

        int size = users.size();

        if(users.isEmpty()) {
            return "It's null !";
        }

        log.info("{}", users.get(0));

        return "Size is " + size + " and, he is " + users.get(0).toString();
    }
}
