package com.zhryua.gpuserservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GpUserServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(GpUserServiceApplication.class, args);
    }



}
