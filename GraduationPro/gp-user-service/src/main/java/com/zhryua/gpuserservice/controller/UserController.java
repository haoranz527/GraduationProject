package com.zhryua.gpuserservice.controller;

import com.zhryua.gpuserservice.service.UserService;
import com.zhryua.userapi.controller.api.UserRegisterApi;
import com.zhryua.userapi.pojo.req.InsertUserRequest;
import com.zhryua.userapi.pojo.req.LoginRequest;
import com.zhryua.userapi.pojo.req.SelectUserListRequest;
import com.zhryua.userapi.pojo.req.UpdateUserRequest;
import com.zhryua.userapi.pojo.resp.BaseResponse;
import com.zhryua.userapi.pojo.resp.LoginResponse;
import com.zhryua.userapi.pojo.resp.SelectUserListResponse;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class UserController implements UserRegisterApi {

    @Autowired
    UserService userService;

    @Override
    public LoginResponse login(LoginRequest request) {
        System.out.println(ToStringBuilder.reflectionToString(request));
        return userService.login(request);
    }

    @Override
    public BaseResponse logout(HttpServletRequest request) {
        return userService.logout(request);
    }

    @Override
    public SelectUserListResponse selectUserPage(SelectUserListRequest request) {
        return userService.selectUserPage(request);
    }

    @Override
    public BaseResponse updateUser(UpdateUserRequest request) {
        return userService.updateUser(request);
    }

    @Override
    public BaseResponse insertUser(InsertUserRequest request) {
        System.out.println(ToStringBuilder.reflectionToString(request));
        return userService.insertUser(request);
    }

    @Override
    public String sqlTest(String id) {
        return userService.sqlTest(id);
    }
}
