package com.zhryua.gpuserservice.mapper;

import com.zhryua.userapi.pojo.database.User;
import com.zhryua.userapi.pojo.database.UserExample;
import org.apache.ibatis.annotations.Mapper;

/**
 * UserMapper继承基类
 */
@Mapper
public interface UserMapper extends MyBatisBaseDao<User, String, UserExample> {
}