关于登录与登出的相关逻辑

- 多点登录，可以同时多个人登录进系统。

- 将登录的用户的userid存入到redis，拦截器在redis中查询。
- redis中的userid设置存储时间，默认为不保存，可设置为保存7天

- 请求登录后，在redis中存储用户的userId，k-v为"userId"-userId
- 过滤的逻辑是：前端向后台返回(String)AUTHORIZATION(userId-token)，AUTHORIZATION在过滤器中进行解析
- 

