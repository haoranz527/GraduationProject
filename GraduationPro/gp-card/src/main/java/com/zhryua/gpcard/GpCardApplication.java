package com.zhryua.gpcard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class GpCardApplication {

    public static void main(String[] args) {
        SpringApplication.run(GpCardApplication.class, args);
    }

}
