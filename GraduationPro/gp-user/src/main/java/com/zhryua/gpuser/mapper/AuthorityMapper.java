package com.zhryua.gpuser.mapper;

import com.zhryua.userapi.pojo.database.Authority;
import com.zhryua.userapi.pojo.database.AuthorityExample;
import org.apache.ibatis.annotations.Mapper;

/**
 * AuthorityMapper继承基类
 */
@Mapper
public interface AuthorityMapper extends MyBatisBaseDao<Authority, String, AuthorityExample> {
}