package com.zhryua.gpuser.utils.json;

import com.alibaba.fastjson.JSONObject;
import com.zhryua.userapi.pojo.database.User;
import com.zhryua.userapi.pojo.req.InsertUserRequest;
import com.zhryua.userapi.pojo.req.LoginRequest;

public class JsonUtils {

    public static String toJson(Object o) {
        return JSONObject.toJSONString(o);
    }

    public static void main(String[] args) {
//        InsertUserRequest request = new InsertUserRequest();
//        request.setUserId("123462717616671625");
//        request.setName("张逸");
//        request.setPhone("12300998877");
//        request.setEmail("null email");
//        request.setPassword("00009");
//        System.out.println(toJson(request));

        LoginRequest request = new LoginRequest();
        request.setPassword("00009");
        request.setUserId("123462717616671625");
        request.setType(0);
        System.out.println(toJson(request));


    }


}
