package com.zhryua.gpuser.mapper;

import com.zhryua.userapi.pojo.database.RoleAndAuthority;
import com.zhryua.userapi.pojo.database.RoleAndAuthorityExample;
import org.apache.ibatis.annotations.Mapper;

/**
 * RoleAndAuthorityMapper继承基类
 */
@Mapper
public interface RoleAndAuthorityMapper extends MyBatisBaseDao<RoleAndAuthority, String, RoleAndAuthorityExample> {
}