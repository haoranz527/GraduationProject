package com.zhryua.gpuser.annotation;

import java.lang.annotation.*;


/**
 * 使用本注解的方法不会进行登录验证
 * @author zhryua
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface NoneAuth {

}
