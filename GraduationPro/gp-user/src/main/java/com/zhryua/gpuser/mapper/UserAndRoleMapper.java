package com.zhryua.gpuser.mapper;

import com.zhryua.userapi.pojo.database.UserAndRole;
import com.zhryua.userapi.pojo.database.UserAndRoleExample;
import org.apache.ibatis.annotations.Mapper;

/**
 * UserAndRoleMapper继承基类
 */
@Mapper
public interface UserAndRoleMapper extends MyBatisBaseDao<UserAndRole, UserAndRole, UserAndRoleExample> {
}