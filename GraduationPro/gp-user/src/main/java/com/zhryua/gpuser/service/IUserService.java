package com.zhryua.gpuser.service;


import com.zhryua.userapi.controller.api.UserRegisterApi;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "gp-user-service")
public interface IUserService extends UserRegisterApi {

}
