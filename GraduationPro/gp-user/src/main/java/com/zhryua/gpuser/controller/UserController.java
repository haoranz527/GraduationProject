package com.zhryua.gpuser.controller;

import com.zhryua.gpuser.annotation.NoneAuth;

import com.zhryua.gpuser.service.IUserService;
import com.zhryua.userapi.pojo.req.InsertUserRequest;
import com.zhryua.userapi.pojo.req.LoginRequest;
import com.zhryua.userapi.pojo.req.SelectUserListRequest;
import com.zhryua.userapi.pojo.req.UpdateUserRequest;
import com.zhryua.userapi.pojo.resp.BaseResponse;
import com.zhryua.userapi.pojo.resp.LoginResponse;
import com.zhryua.userapi.pojo.resp.SelectUserListResponse;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("pub/v1/user")
public class UserController {

    @Autowired
    IUserService userService;

    @GetMapping("/login")
    @NoneAuth
    public LoginResponse login(@RequestBody LoginRequest request) {
        System.out.println(ToStringBuilder.reflectionToString(request));
        return userService.login(request);
    }

    @PostMapping("/logout")
    public BaseResponse logout(HttpServletRequest request) {
        return userService.logout(request);
    }

    @PostMapping("/insert")
    public BaseResponse insertUser(@RequestBody InsertUserRequest request) {
        System.out.println(request.toString());
        return userService.insertUser(request);
    }

    @PostMapping("/update")
    public BaseResponse updateUser(@RequestBody UpdateUserRequest request) {
        return userService.updateUser(request);
    }

    @PostMapping("/select")
    public SelectUserListResponse selectUserList(@RequestBody SelectUserListRequest request) {
        return userService.selectUserPage(request);
    }

    @GetMapping("test")
    public String test() {
        System.out.println("test is ok");
        return "test is ok";
    }

    @PostMapping("/sqlTest")
    public String sqlTest(@RequestParam("id") String id) {
        return userService.sqlTest(id);
    }

}
