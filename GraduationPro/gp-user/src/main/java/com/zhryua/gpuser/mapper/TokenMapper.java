package com.zhryua.gpuser.mapper;

import com.zhryua.userapi.pojo.database.Token;
import com.zhryua.userapi.pojo.database.TokenExample;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * TokenMapper继承基类
 */
@Mapper
@Repository
public interface TokenMapper extends MyBatisBaseDao<Token, String, TokenExample> {
}