package com.zhryua.gpuser.mapper;

import com.zhryua.userapi.pojo.database.Role;
import com.zhryua.userapi.pojo.database.RoleExample;
import org.apache.ibatis.annotations.Mapper;

/**
 * RoleMapper继承基类
 */
@Mapper
public interface RoleMapper extends MyBatisBaseDao<Role, String, RoleExample> {
}