package com.zhryua.gpuser;

import com.zhryua.gpuser.mapper.TokenMapper;
import com.zhryua.userapi.pojo.database.Token;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
public class MainTest {

    @Autowired
    private TokenMapper tokenMapper;

    @Test
    public void insertTokenTest() {
        Token token = new Token();
        token.setToken("token");
        token.setBizId("bizid");
        token.setUserId("userId");
        token.setCtm(new Date());
        token.setUtm(new Date());
        int insert = tokenMapper.insert(token);
        System.out.println(insert);
    }

}
