package com.zhryua.gpuser.service;

import com.zhryua.userapi.pojo.database.User;
import com.zhryua.userapi.pojo.req.InsertUserRequest;
import com.zhryua.userapi.pojo.resp.BaseResponse;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserServiceTest {

    @Autowired
    IUserService userService;

    @Test
    public void sqlTest() {
        System.out.println("OK");
    }

    @Test
    public void insertUserTest() {
        InsertUserRequest request = new InsertUserRequest();
        request.setUserId("1234");
        request.setName("����");
        request.setPhone("12300998877");
        request.setEmail("null email");

        BaseResponse response = userService.insertUser(request);

        if(request == null) {
            System.out.printf("ERROR !");
        }

        System.out.printf(ToStringBuilder.reflectionToString(response));
    }

}
