package com.zhryua.utilsapi.mapper;

import com.zhryua.utilsapi.pojo.database.Card;
import com.zhryua.utilsapi.pojo.database.CardExample;
import org.apache.ibatis.annotations.Mapper;

/**
 * CardMapper继承基类
 */
@Mapper
public interface CardMapper extends MyBatisBaseDao<Card, String, CardExample> {
}