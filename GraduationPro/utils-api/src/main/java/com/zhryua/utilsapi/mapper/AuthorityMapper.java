package com.zhryua.utilsapi.mapper;

import com.zhryua.utilsapi.pojo.database.Authority;
import com.zhryua.utilsapi.pojo.database.AuthorityExample;
import org.apache.ibatis.annotations.Mapper;

/**
 * AuthorityMapper继承基类
 */
@Mapper
public interface AuthorityMapper extends MyBatisBaseDao<Authority, String, AuthorityExample> {
}