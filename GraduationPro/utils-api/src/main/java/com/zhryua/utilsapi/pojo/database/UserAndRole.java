package com.zhryua.utilsapi.pojo.database;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author 
 * 
 */
@Table(name="user_and_role")
@Data
public class UserAndRole implements Serializable {
    @NotEmpty
    private String userId;

    private String roleId;

    private static final long serialVersionUID = 1L;
}