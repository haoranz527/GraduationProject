package com.zhryua.utilsapi.YUAConstants;

/**
 * 存贮常用常量
 * 2021年12月9日16:30:01
 * @author zhryua
 */
public class YUAConstants {

    //常数常量
    public static final Integer ZERO_INT = 0;

    public static final Integer ONE_INT = 1;

    public static final Integer MINUS_ONE_INT = -1;

    public static final Integer TWO_INT = 2;

    public static final Integer THREE_INT = 3;

    public static final Integer FOUR_INT = 4;

    public static final Integer SUCCESS = 6666;

    public static final Integer ERROR = 9999;//内部错误

    public static final long TOKEN_EXPIRES_SECOND = 1800;

    public static final long ONE_DAY = 86400000L;

    public static final long SEVEN_DAY = 604800000L;

    public static final String AUTHORIZATION = "authStr";

    public static final String CURRENT_USER_ID = "userId";

}
