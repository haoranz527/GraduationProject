package com.zhryua.utilsapi.mapper;

import com.zhryua.utilsapi.pojo.database.TradeHistory;
import com.zhryua.utilsapi.pojo.database.TradeHistoryExample;
import org.apache.ibatis.annotations.Mapper;

/**
 * TradeHistoryMapper继承基类
 */
@Mapper
public interface TradeHistoryMapper extends MyBatisBaseDao<TradeHistory, String, TradeHistoryExample> {
}