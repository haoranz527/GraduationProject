package com.zhryua.utilsapi.mapper;

import com.zhryua.utilsapi.pojo.database.UserAndRole;
import com.zhryua.utilsapi.pojo.database.UserAndRoleExample;
import org.apache.ibatis.annotations.Mapper;

/**
 * UserAndRoleMapper继承基类
 */
@Mapper
public interface UserAndRoleMapper extends MyBatisBaseDao<UserAndRole, UserAndRole, UserAndRoleExample> {
}