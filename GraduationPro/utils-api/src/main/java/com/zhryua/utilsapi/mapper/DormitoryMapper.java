package com.zhryua.utilsapi.mapper;

import com.zhryua.utilsapi.pojo.database.Dormitory;
import com.zhryua.utilsapi.pojo.database.DormitoryExample;
import org.apache.ibatis.annotations.Mapper;

/**
 * DormitoryMapper继承基类
 */
@Mapper
public interface DormitoryMapper extends MyBatisBaseDao<Dormitory, String, DormitoryExample> {
}