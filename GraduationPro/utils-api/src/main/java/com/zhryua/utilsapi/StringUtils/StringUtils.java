package com.zhryua.utilsapi.StringUtils;

import sun.misc.BASE64Encoder;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

    /**
     * 判断字符是否为空或空字符串
     * @param str
     * @return 当字符串为 null 或 为 空白、空串 时返回 true
     */
    public static boolean empty(String str) {
        return str == null || str.trim().isEmpty();
    }

    /**
     * 判断字符是否为空或空字符串
     * @param str
     * @return 当 字符串是不是null且不是空串也不是空白时返回 true
     */
    public static boolean notEmpty(String str) {
        return str != null && str.trim().length() > 0;
    }

    /**
     * 判断一个字符串变量是否为 null
     * @param str
     * @return 当 字符串变量 为 null 时返回 true
     */
    public static boolean isNull(String str) {
        return str == null;
    }

    /**
     * 判断一个字符串是否为 空串
     * @param str
     * @return 当字符串中的值是 空串 或 空白 串时返回 true
     */
    public static boolean emptyString(String str) {
        return (str != null) && str.length() == str.trim().length();
    }

    /**
     * 判断一个字符串是否为 空白 串
     * @param str
     * @return 当字符串中的值是 空白 串时返回 true
     */
    public static boolean blank(String str) {
        return (str != null) && str.length() > str.trim().length();
    }

    /**
     * 比较两个非空(不是null，不是空串、不是空白)字符串是否"相等"
     * @param str1
     * @param str2
     * @return 当 两个字符串 都不为空串 且 内容完全一致 (剔除首尾空白后、大小写也一致)时返回 true
     */
    public static boolean equals(String str1, String str2) {
        return equals(str1, str2, true, false);
    }

    /**
     * 比较两个字符串是否 "相等"
     * @param str1
     * @param str2
     * @param escapeSpace 是否需要剔除首尾空白 ( true 表示需要剔除首尾空白，false 表示不剔除 )
     * @param ignoreCase 是否忽略大小写 ( true 表示忽略大小写 ，false 表示不忽略大小写 )
     * @return
     */
    public static boolean equals(String str1, String str2, boolean escapeSpace, boolean ignoreCase ) {
        if(str1 == null || str1 == null) {
            return false;
        }
        if(escapeSpace) {
            str1 = str1.trim();
            str2 = str2.trim();
        }
        return ignoreCase ? str1.equalsIgnoreCase(str2) : str1.equals(str2);
    }

    /**
     * 随机生成一个 32 位长度的 字符串( UUID )
     * @return
     */
    public static String random() {
        UUID uuid = UUID.randomUUID();
        String uuidString = uuid.toString();
        uuidString = uuidString.replace("-","");
        uuidString = uuidString.toUpperCase();
        return uuidString;
    }

    /**
     * 通过 YUA 对字符串进行加密
     * @param str
     * @return
     */
    public static String YUA(String str) {
        try{
            MessageDigest md = MessageDigest.getInstance("sha1");
            BASE64Encoder encoder = new BASE64Encoder();
            return encoder.encode(md.digest(str.getBytes()));
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 对 给定字符串 进行 md5 加密
     * @param str
     * @return
     */
    protected static String md524(String str) {
        try{
            MessageDigest md = MessageDigest.getInstance("MD5");
            BASE64Encoder encoder = new BASE64Encoder();
            return encoder.encode(md.digest(str.getBytes()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 对字符串进行MD5加密
     * @param str source 需要加密的字符串
     * @return 返回加密后的字符串
     */
    public static final String MD5(String str) {
        if(str != null) {
            StringBuffer md5 = new StringBuffer();
            MessageDigest md = null;
            try{
                md = MessageDigest.getInstance("MD5");
                md.update(str.getBytes(StandardCharsets.UTF_8));
                byte[] mdBytes = md.digest();

                for (int i = 0; i < mdBytes.length; i++) {
                    int temp;
                    if(mdBytes[i] < 0) {
                        temp = 256 + mdBytes[i];
                    } else {
                        temp = mdBytes[i];
                    }
                    if(temp < 16) {
                        md5.append("0");
                    }
                    md5.append(Integer.toString(temp, 16));
                }
            }catch(Exception e) {
                e.printStackTrace();
            }
            return md5.toString().toUpperCase();
        }
        return null;
    }

    /**
     * 检测邮箱合法性
     * @param email
     * @return
     */
    public static boolean isEmail(String email) {
        if((email == null) || (email.trim().length() == 0)) {
            return false;
        }
        String regEx = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(email.trim().toLowerCase());
        return m.find();
    }

    /**
     * Double进行四舍五入
     * @param v
     * @param scale
     * @return
     */
    public static double getDouble(Double v, int scale) {
        if(scale < 0) {
            scale = 0;
        }
        BigDecimal b = new BigDecimal(v);
        BigDecimal one = new BigDecimal("1");
        return b.divide(one, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * 得到指定位数的小数
     * @param v
     * @param scale
     * @return
     */
    public static String getDecimals(Double v, int scale) {
        return String.format("%." + String.valueOf(scale) + "f", v);
    }

    /**
     * 根据Unicode编码完美的判断中文汉字和符号
     * @param c
     * @return
     */
    private static boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION) {
            return true;
        }
        return false;
    }

    /**
     * 判断是否含有汉字
     * @param str
     * @return
     */
    public static boolean isChineseHave(String str) {
        char[] ch = str.toCharArray();
        for (int i = 0; i < ch.length; i++) {
            char c = ch[i];
            if(isChinese(c)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断是否只有中文汉字
     * @param str
     * @return
     */
    public static boolean isChineseAll(String str) {
        char[] ch = str.toCharArray();
        for (char c : ch) {
            if(!isChinese(c)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 判断邮政编码
     * @param str
     * @return
     */
    public static boolean isCard(String str) {
        Pattern p = Pattern.compile("[1-9]\\d{5}(?!\\d)");
        Matcher m = p.matcher(str);
        return m.matches();
    }
}
