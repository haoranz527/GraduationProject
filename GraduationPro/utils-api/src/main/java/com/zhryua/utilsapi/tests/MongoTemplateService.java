package com.zhryua.utilsapi.tests;

import com.zhryua.utilsapi.pojo.database.Role;
import com.zhryua.utilsapi.pojo.database.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

@Service
public class MongoTemplateService {

    @Autowired
    public MongoTemplate mongoTemplate;

    public void saveObject(Object obj){
        mongoTemplate.save(obj);
    }

    public void saveUser(User user) {
        mongoTemplate.save(user);
    }

    public void saveRole(Role role) {
        mongoTemplate.save(role);
    }
}
