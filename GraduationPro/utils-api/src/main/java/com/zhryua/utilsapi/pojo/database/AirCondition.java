package com.zhryua.utilsapi.pojo.database;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author 
 * 
 */
@Table(name="air_condition")
@Data
public class AirCondition implements Serializable {
    @Id
    @GeneratedValue
    private String bizId;

    private Short status;

    private String userId;

    private Short repairs;

    private Date ctm;

    private Integer useTime;

    private Double power;

    private String powerId;

    private static final long serialVersionUID = 1L;
}