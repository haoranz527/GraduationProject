package com.zhryua.utilsapi.StringUtils;

import lombok.extern.slf4j.Slf4j;

import java.security.MessageDigest;

@Slf4j
public class MD5Utils {

    /**
     * MD5加密 生成32位加密码
     * @param inStr
     * @return
     */
    public static String string2MD5(String inStr) {
        MessageDigest md5 = null;
        try{
            md5 = MessageDigest.getInstance("MD5");
        } catch (Exception e) {
            log.error("加密失败，{}", e);
            return "";
        }
        char[] charArry = inStr.toCharArray();
        byte[] byteArray = new byte[charArry.length];

        for (int i = 0; i < charArry.length; i++) {
            byteArray[i] = (byte)charArry[i];
        }
        byte[] md5Bytes = md5.digest(byteArray);
        StringBuffer hexValue = new StringBuffer();
        for (int i = 0; i < md5Bytes.length; i++) {
            int val = ((int) md5Bytes[i]) & 0xff;
            if(val < 16) {
                hexValue.append("0");
            }
            hexValue.append(Integer.toHexString(val));
        }
        return hexValue.toString();
    }

    /**
     * 加密解密算法 执行一次加密，两次解密
     * @param inStr
     * @return
     */
    public static String convertMD5(String inStr) {
        char[] a = inStr.toCharArray();
        for (int i = 0; i < a.length; i++) {
            a[i] = (char)(a[i] ^ 't');
        }
        String s = new String(a);
        return s;
    }

    /**
     * 断输入的密码和数据库中保存的MD5密码是否一致
     * @param inputPassword inputPassword 输入的密码
     * @param md5DB 数据库保存的密码
     * @return
     */
    public static boolean passwordIsTrue(String inputPassword, String md5DB) {
        String md5 = string2MD5(inputPassword);
        return md5DB.equals(md5);
    }


    public static void main(String[] args) {
        String s = "zhryua";
        log.info("原始:" + s);
        log.info("MD5:" + string2MD5(s));
//        System.out.println("是否一致:" + passwordIsTrue());
        System.out.println(passwordIsTrue(s, "64b1157a20cb0c4c16408acee0fa684c"));
    }
}
