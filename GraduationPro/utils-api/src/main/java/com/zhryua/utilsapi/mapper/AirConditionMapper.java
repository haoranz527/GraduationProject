package com.zhryua.utilsapi.mapper;

import com.zhryua.utilsapi.pojo.database.AirCondition;
import com.zhryua.utilsapi.pojo.database.AirConditionExample;
import org.apache.ibatis.annotations.Mapper;

/**
 * AirConditionMapper继承基类
 */
@Mapper
public interface AirConditionMapper extends MyBatisBaseDao<AirCondition, String, AirConditionExample> {
}