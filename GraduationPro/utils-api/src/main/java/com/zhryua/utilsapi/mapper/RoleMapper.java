package com.zhryua.utilsapi.mapper;

import com.zhryua.utilsapi.pojo.database.Role;
import com.zhryua.utilsapi.pojo.database.RoleExample;
import org.apache.ibatis.annotations.Mapper;

/**
 * RoleMapper继承基类
 */
@Mapper
public interface RoleMapper extends MyBatisBaseDao<Role, String, RoleExample> {
}