package com.zhryua.utilsapi.mapper;

import com.zhryua.utilsapi.pojo.database.Token;
import com.zhryua.utilsapi.pojo.database.TokenExample;
import org.apache.ibatis.annotations.Mapper;

/**
 * TokenMapper继承基类
 */
@Mapper
public interface TokenMapper extends MyBatisBaseDao<Token, String, TokenExample> {
}