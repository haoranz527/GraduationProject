package com.zhryua.utilsapi.mapper;

import com.zhryua.utilsapi.pojo.database.RoleAndAuthority;
import com.zhryua.utilsapi.pojo.database.RoleAndAuthorityExample;
import org.apache.ibatis.annotations.Mapper;

/**
 * RoleAndAuthorityMapper继承基类
 */
@Mapper
public interface RoleAndAuthorityMapper extends MyBatisBaseDao<RoleAndAuthority, String, RoleAndAuthorityExample> {
}