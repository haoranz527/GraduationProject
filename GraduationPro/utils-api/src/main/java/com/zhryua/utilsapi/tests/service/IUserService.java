package com.zhryua.utilsapi.tests.service;

import com.zhryua.utilsapi.pojo.database.User;
import org.springframework.stereotype.Service;

@Service
public interface IUserService {

    Integer insertUser(User user);


}
