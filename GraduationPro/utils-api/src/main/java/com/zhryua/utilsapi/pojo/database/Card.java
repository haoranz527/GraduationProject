package com.zhryua.utilsapi.pojo.database;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author 
 * 
 */
@Table(name="card")
@Data
public class Card implements Serializable {
    @Id
    @GeneratedValue
    private String bizId;

    private Integer status;

    private String userId;

    private Date mtm;

    private Date atm;

    private Double limit;

    private Double balance;

    private static final long serialVersionUID = 1L;
}