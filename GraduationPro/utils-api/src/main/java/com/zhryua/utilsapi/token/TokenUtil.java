package com.zhryua.utilsapi.token;

import java.util.UUID;

/**
 * 根据实际使用情况确定token生成器，此处为简单实现
 */
public class TokenUtil {

    public static String getToken() {
        return UUID.randomUUID().toString();
    }

}
