package com.zhryua.utilsapi.tests.service.impl;

import com.zhryua.utilsapi.YUAConstants.YUAConstants;
import com.zhryua.utilsapi.pojo.database.User;
import com.zhryua.utilsapi.mapper.UserMapper;
import com.zhryua.utilsapi.tests.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper userMapper;

    /**
     * 新增一位用户
     * @param user
     * @return
     */
    @Override
    public Integer insertUser(User user) {
        log.info("#####insertUser入参{}#####", user);
        try{
            int i = userMapper.insert(user);

            if(i > 0) {
                log.info("#####insertUser插入成功{}#####", user);

                return YUAConstants.ONE_INT;
            } else {
                return YUAConstants.TWO_INT;
            }
        } catch(Exception e) {
            log.error("#####insertUser{}#####",e);

            return YUAConstants.ERROR;
        }
    }

}
