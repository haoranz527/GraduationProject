package com.zhryua.utilsapi.mapper;

import com.zhryua.utilsapi.pojo.database.User;
import com.zhryua.utilsapi.pojo.database.UserExample;
import org.apache.ibatis.annotations.Mapper;

/**
 * UserMapper继承基类
 */
@Mapper
public interface UserMapper extends MyBatisBaseDao<User, String, UserExample> {
}