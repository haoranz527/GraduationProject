package com.zhryua.utilsapi.pojo.database;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author 
 * 
 */
@Table(name="trade_history")
@Data
public class TradeHistory implements Serializable {
    @Id
    @GeneratedValue
    private String bizId;

    private Date etm;

    private Double amount;

    private String item;

    private Integer consumption;

    private Integer status;

    private String address;

    private String processorId;

    private static final long serialVersionUID = 1L;
}