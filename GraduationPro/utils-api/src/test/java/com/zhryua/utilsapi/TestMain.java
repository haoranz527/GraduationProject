package com.zhryua.utilsapi;

import com.zhryua.utilsapi.StringUtils.IdWorker;
import com.zhryua.utilsapi.StringUtils.StringUtils;
import com.zhryua.utilsapi.pojo.database.Role;
import com.zhryua.utilsapi.pojo.database.User;
import com.zhryua.utilsapi.tests.MongoTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
@Slf4j
public class TestMain {

    @Autowired
    private MongoTemplateService mongoTemplateService;

    private static IdWorker idWorker = new IdWorker();

    @Test
    void test_1(){
        log.info("test_1");
        System.out.println("--");
    }

    @Test
    void test_2() {
        log.info("test_2");
        String str = "zhryua";
        System.out.println("str :" + str);
        String s = StringUtils.YUA(str);
        System.out.println("s :" + s);
    }

    @Test
    void test_3() {
        log.info("test_3");
        String str = "zhryua";
        System.out.println("str :" + str);
        String s = StringUtils.MD5(str);
        System.out.println("s :" + s);
    }

    @Test
    void test_4() {
        Date date = new Date();
        System.out.println(new Date());
        System.out.println(date.getDay());
    }

    @Test
    void test_5() {
        IdWorker idWorker = new IdWorker();
        for (int i = 0; i < 20; i++) {
            System.out.println(idWorker.nextId());
        }
    }

    @Test
    void test_6_mongotemplate() {
        User user = new User();
        user.setBizId(idWorker.nextId() + "");
        user.setName("zhryua");
        mongoTemplateService.saveUser(user);

        Role role = new Role();
        role.setName("学生");
        role.setBizId(idWorker.nextId() + "");
        mongoTemplateService.saveRole(role);

        user.setBizId(idWorker.nextId() + "");
        mongoTemplateService.saveObject(user);

    }


}
