package com.zhryua.utilsapi.redis;

import com.zhryua.utilsapi.pojo.database.User;
import com.zhryua.utilsapi.redis.impl.RedisUtilImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootTest
public class RedisUtilImplTest {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private RedisUtilImpl redisUtil;

    @Test
    public void setTest() {
        redisUtil.setRedisTemplate(redisTemplate);
        User user = new User();
        user.setName("YUA");
        user.setCtm(new Date());
        boolean b = redisUtil.set(user.getName(), user);
        if(b) {
            System.out.println("OK");
        } else {
            System.out.println("Error");
        }
    }

    @Test
    public void lSetTest() {
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            list.add((int) (Math.random() * 1000));
        }

        redisUtil.setRedisTemplate(redisTemplate);
        boolean b = redisUtil.lSet("Test-List", list);

        if(b) {
            System.out.println("Test SUCCESS");
        } else {
            System.err.println("Test ERROR !");
        }
    }

    @Test
    public void lGetTest() {
        redisUtil.setRedisTemplate(redisTemplate);

        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            list.add((int) (Math.random() * 1000));
        }

        String key = "Integer-List";

        if (redisUtil.hasKey(key)) {
            redisUtil.del(key);
        }

        boolean b = redisUtil.lSet(key, list);

        if(b) {
            System.out.println("Set Success !");
        } else {
            System.out.println("Set Error");

            return;
        }

        List<Object> lGet = redisUtil.lGet(key, 0, -1);

        List<Integer> o = (List<Integer>) lGet.get(0);

        for (int i = 0; i < o.size(); i++) {
            System.out.println(i + ": o.get(i) is " + o.get(i) + ", list.get(i) is " + list.get(i));
            int compare = Integer.compare(o.get(i), list.get(i));
            if(compare != 0) {
                System.err.println("Error !");
            }
        }

        System.out.println("Test Success");

    }

}
