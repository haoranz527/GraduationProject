package com.zhryua.utilsapi.tests.service;

import com.zhryua.utilsapi.StringUtils.IdWorker;
import com.zhryua.utilsapi.pojo.database.User;
import com.zhryua.utilsapi.tests.service.impl.UserServiceImpl;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserServiceImpTest {

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private UserServiceImpl userService;

    @Test
    public void insertUserTest() {
        User user = new User();

        user.setBizId(idWorker.nextId() + "");

        user.setName("zhryua");

        user.setCardId("C"+idWorker.nextId());

        Object o = userService.insertUser(user);

        System.out.println(ToStringBuilder.reflectionToString(o));
    }

    @Test
    public void insertUserTest_2() {
        User user = new User();

        for (int i = 0; i < 100; i++) {
            user.setBizId(idWorker.nextId() + "");

            user.setName("zhryua");

            user.setCardId("C"+idWorker.nextId());

            Object o = userService.insertUser(user);

            System.out.println(ToStringBuilder.reflectionToString(o));
        }
    }

    @Test
    void testprint(){
        System.out.println("hello !");
    }

//    @Test
//    public void selectUserPageTest() {
//        Integer pageNum = 1;
//        Integer pageSize = 20;
//
//        List<User> users = userService.selectUserPage(pageNum, pageSize);
//
//        if(users.size() > 0) {
//            for (User user : users) {
//                System.out.println(user.toString());
//            }
//        } else {
//            System.err.println("ʧ�ܣ�����");
//        }
//    }
//
//    @Test
//    public void selectUserPageTest_2() {
//        Integer pageNum = 1;
//        Integer pageSize = 20;
//
//        List<User> users = userService.selectUserPage(pageNum, pageSize);
//
//        if(users.size() > 0) {
//            for (User user : users) {
//                System.out.println(user.getBizId());
//            }
//        } else {
//            System.err.println("ʧ�ܣ�����");
//        }
//    }
//
//    @Test
//    public void selectUserPageInfo() {
//        Integer pageNum = 1;
//        Integer pageSize = 20;
//
//        PageInfo<User> pageInfo = userService.selectUserPageInfo(pageNum, pageSize);
//
//        System.out.println("pageInfo.getSize() :"+pageInfo.getSize());
//
//        System.out.println("pageInfo.getNextPage() :"+pageInfo.getNextPage());
//
//        System.out.println("pageInfo.getNavigateFirstPage() :"+pageInfo.getNavigateFirstPage());
//
//        System.out.println("pageInfo.getNavigateLastPage() :"+pageInfo.getNavigateLastPage());
//
//        System.out.println("pageInfo.getNavigatePages() :"+pageInfo.getNavigatePages());
//
//        System.out.println("pageInfo.getNavigatepageNums() :"+pageInfo.getNavigatepageNums());
//
//        System.out.println("pageInfo.getPageNum() :" +pageInfo.getPageNum());
//
//    }

}
